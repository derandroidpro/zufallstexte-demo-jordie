package beta.test.zufallstextedemoderandroidpro;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String[] zufallstexte = getResources().getStringArray(R.array.zufallstexte_array);

        final TextView textView = (TextView) findViewById(R.id.textView);
        Button btn = (Button) findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String aktuellerText = zufallstexte[new Random().nextInt(zufallstexte.length)];
                textView.setText(aktuellerText);

            }
        });
    }

}
